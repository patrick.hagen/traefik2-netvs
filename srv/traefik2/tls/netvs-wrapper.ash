#!/bin/ash

set -e

# TODO: enter path to dehydrated_netvs_hook
export ACME4NETVS="/acme4netvs/dehydrated_netvs_hook"

if [ ! -x "${ACME4NETVS}" ]; then
  echo "Unable to find dehydrated_netvs_hook"
  exit 1
fi

if [ $# -lt 3 ]; then
  echo "Not enough arguments given"
  exit 2
fi

COMMAND="$1"
DOMAIN="$2"
CHALLENGE="$3"

# strip _acme-challenge. prefix
DOMAIN=${DOMAIN/#_acme-challenge./}
DOMAIN=`echo $DOMAIN | sed -e 's/^_acme-challenge.//'`
echo $DOMAIN | tee -a /tls/netvs.log
case $COMMAND in
  "present")
exec "${ACME4NETVS}" deploy_challenge "${DOMAIN}" "ignore" "${CHALLENGE}"  | tee -a /tls/netvs.log
  ;;
  "cleanup")
    exec "${ACME4NETVS}" clean_challenge "${DOMAIN}" "ignore" "${CHALLENGE}"  | tee -a /tls/netvs.log
  ;;
  *)
    echo "Unknown command $1."
    exit 0
esac
