# Traefik mit ACME/Letsencrypt und DNS-Challenge über netvs am kit

## Ziel

Traefik läuft als Docker-Container und arbeitet als Reverse-Proxy für andere Container.

## Vorbedingungen

Das Beispiel ist unter Ubuntu getestet und sollte unter Debian funktionieren. Auf anderen Systemen sollte sich nur die Installation von acme2netvs unterscheiden.

Docker ist installiert, acme4netvs ist im Standard-Pfad installiert (https://pad.net.scc.kit.edu/pqgiXyThRJa0xIjoiUU-ow#)

Ein docker-Netwerk "publicweb" ist eingerichtet.

```
 docker network create publicweb
```

Die nötigen Konten für den netvs-Zugrif sind eingerichtet (https://docs.ca.kit.edu/acme4netvs/en/)

## Beispiel starten

Die Traefik-Konfiguration und ein (schreibbares) Verzeichnis zum speichern der Zertifikate wird unter /srv/traefik2 erwartet. In traefik.toml ist die E-Mail-Adresse für letsencrypt anzupassen.

Unter docker/traefik muss traefik.env angepasst werden, hier wird der API-Key hinterlegt.

Wird nicht /srv/traefik2 verwendet, so muss docker-compose.yml entsprechend angepasst werden.

Anschließend kann der traefik-Container gestartet werden.

```
cd docker/traefik
docker-compose up -d
```

Zertifikate werden noch nicht beantragt, es läuft ja noch nichts hinter dem Reverse-Proxy.

In docker/whoami muss die Host-Regel angepasst werden. Wichtig: der Name muss zu den Berechtigungen des API-Keys im netvs passen.

Anschließend kann der whoami-Container gestartet werden.

```
cd docker/whoami
docker-compose up -d
```

traefik erkennt automatisch den neuen Container und bindet ihn gemäß Host-rule ein. Automatisch startet die DNS-Challenge.

Ein log findet sich in /srv/traefik2/tls/netvs.log

